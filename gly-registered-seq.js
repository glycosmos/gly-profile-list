import {LitElement, html, css} from 'lit-element';
import getdomain from 'gly-domain/gly-domain.js';


class GlyRegisteredSeq extends LitElement {
  static get properties() {
    return {
      sampleids: Array,
      RegisteredSeq: String,
      userHash: String,
      hashKey: String
    };
  }

  static get styles() {
    return css `
      .glResultWurcs_text {
      	margin: 0 10px;
      	padding: 10px 5px;
      	border-top: dotted 1px #CCC;
      }
    `;
  }
  render() {
    return html `
    	<!-- <p class="glResultWurcs_text">${this._processHtml()}</p> -->
    	<div class="glResultWurcs_text">${this._processHtml()}</div>
   `;
  }

  constructor() {
    super();
    console.log("constructor");
    this.accession="G54245YQ";
    this.image="";
    this.sampleids=[];
    this.mass=null;
    this.contributionTime=null;
    this.hashKey="b607de4aef1874db731e180c7db129c7610da810844d00fa304569ba30cfa98f";
  }

  connectedCallback() {
    super.connectedCallback();
    console.log("cc");
    // const url1 = 'https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_summary?accNum=' + this.accession;
    // const url2 = 'https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_image?accession=' + this.accession + '&style=normalinfo&notation=snfg&format=svg';
    // const url = 'https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_hash_list_by_user_hash?user_hash=' + this.userHash + '&limit=10&offset=0';
const host =  getdomain(location.ref);
    const url = 'https://'+host+'/sparqlist/api/gtc_accession_textseq?hash=' + this.hashKey;
    this.getRegisteredSeq(url);
    console.log(this.hashKey);
  }


  getRegisteredSeq(url) {
    console.log(url);
    var urls = [];

    urls.push(url);
    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("hash-key");
      console.log(JSON.stringify(myJson));
      return myJson;
    }));
    Promise.all(promises).then(results => {
      console.log("values");
      console.log(results);

      this.RegisteredSeq = results.pop();
    });
  }

  _processHtml() {
    if (this.RegisteredSeq.length > 0) {
      console.log("this.RegisteredSeq");
      console.log(this.RegisteredSeq);
      const hashList = this.RegisteredSeq.map(item => {
        if(item.sequence.indexOf('RES') != -1 ) {
          return html`
            <!-- GlycoCT -->
            <pre>${item.sequence.replace(/\r\n/g, '\n').replace(/(\r|\n)/g, '\n')}</pre>
          `;

        } else {
          return html`
            <!-- WURCS etc... -->
            <data>${item.sequence}</data>
          `;
        }
      });
      console.log('hashList');
      console.log(hashList);
      return hashList
    } else {
      return html`Could not retrieve accession & sequence`;
    }
  }

}

customElements.define('gly-registered-seq', GlyRegisteredSeq);
