import { LitElement, html } from 'lit-element';
import getdomain from 'gly-domain/gly-domain.js';
import 'lit-element-bootstrap';

class GlyNormalizedWurcs extends LitElement {
  static get properties() {
    return {
      sampleids: Array,
      hashkey: String
    };
  }

  constructor() {
    super();
    console.log("constructor");
    this.hashkey="";
    this.sampleids=[];
  }

  render() {
    return html `
      <style>
        .message {
          text-align: right;
        }
        .nothingFound {
          width: 100%;
          float: none;
          margin: 0 0 10px;
          padding: 5px 0;
          background: #EEE;
          color: #999;
          font-size: 14px;
          font-weight: bold;
          text-align: center;
          box-sizing: border-box;
        }
      </style>
      <div>${this._processHtml()}</div>
    `;
  }

  connectedCallback() {
    super.connectedCallback();
    console.log("cc");
const host =  getdomain(location.href);
    const url1 = 'https://'+host+'/sparqlist/api/gtc_normalized_wurcs_by_hashkey?hashKey=' + this.hashkey;
    this.getContents(url1);
  }

  getContents(url1) {
    console.log(url1);
    var urls = [];
    urls.push(url1);
    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("summary");
      console.log(JSON.stringify(myJson));
      return myJson;
    }));
    Promise.all(promises).then(results => {
      console.log("values");
      console.log(results);
      this.sampleids = results.pop();
    });
  }

  _processHtml() {
    if (this.sampleids.length > 0) {
      // var popped = this.sampleids.pop();
      console.log(this.sampleids.length);
      const wurcsContents = this.sampleids.map(item => {
        return html`
        <p>${item.NormalizedWurcs}</p>
        `;
      });
      return wurcsContents;
    } else {
      return html`<div class="nothingFound">Processing  of this entry on a server has not been completed yet</div>`;
    }
  }

}

customElements.define('gly-normalized-wurcs', GlyNormalizedWurcs);
