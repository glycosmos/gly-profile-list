import { LitElement, html, css } from 'lit-element';
import './gly-registered-seq.js';
import './gly-registration-date.js';
import getdomain from 'gly-domain/gly-domain.js';
import './gly-error-download.js';
import './gly-acc-download.js';
import './gly-partnerid-download.js';
import 'glycosmos-layout/js/glycosmos-table.js';
import 'glycosmos-layout/js/glytoucan-image.js';



class GlyProfileList extends LitElement {
  static get properties() {
    return {
      dContents: Object,
      sampleids: Array,
      accession: String,
      userHash: String,
      counts: Array,
      limit: Number,
      offset: {
        type: String,
        reflect: true
      },
      hashKeys: Array,
      format: String,
      notation: String,
      url: String,
      page: Number,
      dUrl: String,
      host: String,
      gtcVersion: String
    };
  }
  constructor() {
    super();
    console.log("constructor");
    this.gtcVersion="";
    this.accession="";
    this.image="";
    this.sampleids=[];
    this.mass=null;
    this.contributionTime=null;
    this.userHash="";
    this.format="png";
    this.notation="snfg";
    this.limit=10;
    this.offset=0;
    this.url="";
    this.page=1;
    this.dUrl= "";
    this.host = "";
  }
  static get styles() {
    return css `
      .num-line {
    	  margin: 20px 0 20px;
      }
      .item-num li {
        list-style: none;
      }
      .item-num li > span{
        font-weight: bold;
      }
    `;
  }
  render() {
    return html `
      <bs-container>
          <bs-row>
              <bs-column sm>
                  <bs-list-group>
                      <bs-list-group-badge-item>Number of submission<bs-badge pill primary>${this.counts[0].submissions}</bs-badge></bs-list-group-badge-item>
                      <bs-list-group-badge-item>Number of accession number<bs-badge pill primary>${this.counts[0].accessions}</bs-badge></bs-list-group-badge-item>
                  </bs-list-group>
              </bs-column>
              <bs-column sm>
                  <gly-acc-download userHash="${this.userHash}" gtcVersion="${this.gtcVersion}"></gly-acc-download>
                  <gly-partnerid-download userHash="${this.userHash}" gtcVersion="${this.gtcVersion}"></gly-partnerid-download>
              </bs-column>
              <bs-column sm>
                  <gly-error-download userHash="${this.userHash}" gtcVersion="${this.gtcVersion}"></gly-error-download>
              </bs-column>
          </bs-row>
      </bs-container>

      <glycosmos-table
      listSparqlet="https://${this.host}/sparqlist/api/gtc_submissions_list_by_user_hash?user_hash=${this.userHash}"
      columns='{
        "date_time": {
          "name": "Date",
          "type": "date"
        },
        "hash_key": {
          "name": "Submission Ref",
          "type": "str"
        },
        "sequence": {
          "name": "Sequence",
          "type": "sortOnly"
        },
        "Accession_Number": {
          "name": "Accession Number",
          "type": "image"
        },
        "Server_Log": {
          "name": "Server Log",
          "type": "JSON"
        }
      }'
      orderBool
      hiddenColumns='["Server_Log"]'
      imageApi="https://api.glycosmos.org/wurcs2image/latest/png/binary/">
    </glycosmos-table>
   `;
  }
  connectedCallback() {
    super.connectedCallback();
    console.log("cc");
    this.host =  getdomain(location.href);
    const url = 'https://'+ this.host +'/sparqlist/api/gtc_count_registration_by_user_hash?user_hash=' + this.userHash + '&limit=' + this.limit;
    this.getCounts(url);
  }
  getCounts(url) {
    console.log(url);
    var urls = [];
    urls.push(url);
    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("user-hash");
      console.log(JSON.stringify(myJson));
      return myJson;
    }));
    Promise.all(promises).then(results => {
      console.log("values");
      console.log(results);
      this.counts = results.pop();
    });
  }

}

customElements.define('gly-profile-list', GlyProfileList);
