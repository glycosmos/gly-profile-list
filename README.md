# gly-profile-List

# Components

GitLab CI works on this project so check the Gitlab [Page]().

# Tutorial

Install node packages.
```
$ npm i
```

Install live-server
```
$ npm -g i live-server
$ live-server  & webpack
```

## Update each webcomponents
* if each webcomponents version updated, needs run `npm update`
* needs to update this webcomponent version in `package.json`.


# Add to the test env  
* you MUST run the dev-gly-gtc-component pipline.
* https://gitlab.com/glycosmos/cdn/dev-gly-gtc-component/pipelines


# Tips
Usable :
```
import `lit-element-bootstrap/components/card';``

import { Parser } from 'json2csv';
```

Error :
```
import '@lit-element-bootstrap/card';

const { Parser } = require('json2csv');
```
