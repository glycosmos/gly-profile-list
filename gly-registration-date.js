import {LitElement, html} from 'lit-element';
import getdomain from 'gly-domain/gly-domain.js';


class GlyRegistrationDate extends LitElement {
  static get properties() {
    return {
      sampleids: Array,
      userHash: String,
      hashKey: String,
      registrationDate: String
    };
  }

  render() {
    return html `
    <style>
      padding: 10px;
      margin-bottom: 10px;
    </style>
    <div>${this._processHtml()}<div>
   `;
  }

  // </div>
  // <div>
  // sampleids: ${this.sampleids}
  // accession: ${this.accession}
  // accessionhtml: <p>Accession Number:${this._accessionHtml()}</p>
  // ${this._massHtml()}
  // ${this._contributionHtml()}

  constructor() {
    super();
    console.log("constructor");
    this.sampleids=null;
    this.userHash='';
    this.hashKey= '';
  }

  connectedCallback() {
    super.connectedCallback();
    const host =  getdomain(location.href);
    const url = 'https://'+host+'/sparqlist/api/gtc_registration_date_by_hash?user_hash=' + this.userHash + '&hash_key=' + this.hashKey ;
    this.getContents(url);
  }

  async getContents(url) {
    await fetch(url,{
      mode: 'cors'
    }).then(res => {
      if(res.ok) {
        return res.json();
      } else {
        throw new Error();
      }
    }).then(resArr => {
      this.sampleids = resArr;
    }).catch(err => {
      this.sampleids = null;
    });
    await this.requestUpdate();

    // var urls = [];
    // urls.push(url1);
    // var promises = urls.map(url => fetch(url, {
    //   mode: 'cors'
    // }).then(function (response) {
    //   return response.json();
    // }).then(function (myJson) {
    //   return myJson;
    // }));
    // Promise.all(promises).then(results => {
    //   this.sampleids = results.pop();
    // });
  }

  _processHtml() {
    if (this.sampleids !== null) {
      var popped = this.sampleids.pop();
      var date = popped.date;
      date = new Date(date.split(" ")[0]).toISOString();
      console.log("> date_1: " + date);
      date = date.split("T")[0];
      console.log("> date_2: " + date);
      return html`
        <p>${date}</p>
      `;
    } else {
      return html`Could not retrieve`;
    }
  }

}

customElements.define('gly-registration-date', GlyRegistrationDate);
