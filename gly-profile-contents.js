import { LitElement, html, css } from 'lit-element';
import 'gly-image/gly-image.js';
import './gly-registered-seq.js';
import './gly-registration-date.js';
import 'gly-validated-message/gly-gtc-vm-hashkey.js';
import './gly-normalized-wurcs.js';
import getdomain from 'gly-domain/gly-domain.js';


class GlyProfileContents extends LitElement {
  static get properties() {
    return {
      sampleids: Array,
      accession: String,
      userHash: String,
      limit: String,
      offset: {
        type: String,
        reflect: true
      },
      hashKeys: Array,
      format: String,
      notation: String,
      url: String,
      page: Number
    };
  }

  static get styles() {
    return css `
    .glResult {
    	padding: 0 0 70px;
    }

    .glResultStructure {
    	padding: 0;
    	width: 100%;
    	margin: 0 0 20px;
    	border: solid 2px #CCC;
    	border-collapse: collapse;
    }

    .glResultStructure.glResult--showing {
    	display: table;
    }

    .glResultStructure_header th {
    	color: #999;
    	padding: 5px;
    	border: solid 1px #CCC;
    	border-bottom: solid 2px #CCC;
    	background: #F5F5F5;
    }

    .glResultWurcs_body td {
    	padding: 5px;
    	border: solid 1px #CCC;
    }

    .glResultStructure_acc {
    	width: 195px;
    	text-align: center;
    }

    .glResultStructure_box {
    	position: relative;
    	overflow: visible;
    }
    .glResultStructure_text {
    	/* margin: 0 10px;
    	padding: 10px 5px; */
    	border-top: dotted 1px #CCC;
    }

    .glResultStructure_date {
    	width: 100px;
    	text-align: center;
    }

    .glResultStructure_validBox {
    	width: 300px;
    	position: relative;
    	overflow: visible;
    }

    `;
  }
  render() {
    return html `
    ${this._processHtml()}
   `;
  }


  constructor() {
    super();
    console.log("constructor");
    this.accession="";
    this.image="";
    this.sampleids=[];
    this.mass=null;
    this.contributionTime=null;
    this.userHash="";
    this.format="png";
    this.notation="snfg";
    this.limit=10;
    this.offset=0;
    this.url="";
    this.page=1;
  }

  connectedCallback() {
    super.connectedCallback();
    console.log("cc");

    // host: test.sparqlist.glycosmos.org or sparqlist.glycosmos.org
    const host =  getdomain(location.href);
    this.url = 'https://'+host+'/sparqlist/api/gtc_hash_list_by_user_hash?user_hash=' + this.userHash + '&limit=' + this.limit + '&offset=' + this.offset;
    this.getHashkey(this.url);
    console.log(this.userHash);
  }

  getHashkey(url) {
    console.log(url);
    var urls = [];

    urls.push(url);
    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("user-hash");
      console.log(JSON.stringify(myJson));
      return myJson;
    }));
    Promise.all(promises).then(results => {
      console.log("values");
      console.log(results);

      this.hashKeys = results.pop();
    });
  }

  _processHtml() {
    if (this.hashKeys.length > 0) {
      console.log("this.hashKeys");
      console.log(this.hashKeys);
      let url = location.href;
      console.log('action url:  ' + url);  // url: http://127.0.0.1:8080/public/#test?offset=200
      return html`
        <!-- Table -->
        <table class="glResult glResultStructure" data-mode="wurcs">
          <thead class="glResultStructure_header">
            <tr>
              <th>Submission Ref</th>
              <th class="glResultStructure_acc">Accession Number</th>
              <th>Sequence</th>
              <th>Date</th>
              <th>Validation</th>
              <th>Normalized WURCS</th>
            </tr>
          </thead>
          <tbody class="glResultWurcs_body">
            ${this.hashKeys.map(item => html `
            <tr>
              <td> <!-- Hash key (Registration ID) -->
                <a href="/Structures/Submission/${item.hash_key}">${item.hash_key.substr(0, 10) + "..."}</a>
              </td>
	            <td class="glResultStructure_acc"> <!-- Accession number  -->
                ${this.getAccession(item.accession)}
              </td>
              <td> <!-- Image & Sequence -->
            		<div class="glResultStructure_box">
                  <gly-image accession="${item.accession}" format="${this.format}" notation="${this.notation}"></gly-image>
                  <div class="glResultStructure_text">${this.getSequence(item.sequence)}</div>
            		</div>
              </td>
              <td class="glResultStructure_date"> <!-- Registration date -->
                ${this.getDateTime(item.date_time)}
              </td>
              <td> <!-- Validation Result -->
                <div class="glResultStructure_validBox">
                  <gly-gtc-vm-hashkey hashKey="${item.hash_key}"></gly-gtc-vm-hashkey>
                </div>
              </td>
              <td> <!-- Normalized Wurcs -->
                <div class="glResultStructure_validBox">
                  <gly-normalized-wurcs hashkey="${item.hash_key}"></gly-normalized-wurcs>
                </div>
              </td>
            </tr>
            `)}
          </tbody>
        </table><!--/.glResultStructure-->
      `;
    } else {
      return html`
        <p>Could not retrieve hash list</p>`;
    }
  }

  getAccession(accession) {
    console.log("accession > " + accession);
    if (accession === undefined) {
      console.log("undefined > " + undefined);
      return html`Could not retrieve accession`;
    } else {
      return html`
        <a href="/Structures/Glycans/${accession}" target="_blank">${accession}</a>
      `;
    }
  }

  getSequence(sequence) {
    console.log("sequence > " + sequence);
    if (sequence !== undefined) {
      if(sequence.indexOf('RES') != -1 ) {
        return html`
          <!-- GlycoCT -->
          <pre>${sequence.replace(/\r\n/g, '\n').replace(/(\r|\n)/g, '\n')}</pre>
        `;
      } else {
        return html`
          <!-- WURCS etc... -->
          <data>${sequence}</data>
        `;
      }
    } else {
      return html`Could not retrieve accession & sequence`;
    }
  }

  getDateTime(date_time) {
    if (date_time !== null) {
      let date = new Date(date_time).toUTCString();
      console.log("> date_1: " + date);
      // date = date.split("T")[0];
      // console.log("> date_2: " + date);
      return html`
        <p>${date}</p>
      `;
    } else {
      return html`Could not retrieve`;
    }
  }

}

customElements.define('gly-profile-contents', GlyProfileContents);
