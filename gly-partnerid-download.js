import {LitElement, html, css} from 'lit-element';
import getdomain from 'gly-domain/gly-domain.js';
import { Parser } from 'json2csv';

class GlyPartneridDownload extends LitElement {
  static get properties() {
    return {
      userHash: String,
      url: String,
      host: String,
      isPartner: Boolean,
      gtcVersion: String
    };
  }
  constructor() {
    super();
    this.userHash="";
    this.url="";
    this.host = "";
    this.gtcVersion="";
  }
  static get styles() {
    return css `
      h5 {
        font-size: 1.25rem;
        margin-top: 0.5rem;
        margin-bottom: 0.5rem;
        font-family: inherit;
        line-height: 1.2;
        color: iherit;
      }
      p {
        margin-top:0px;
        margin-bottom: 1rem;
      }
    `;
  }
  render() {
    return html `
    ${this._processHtml()}
   `;
  }
  _processHtml() {
    if(this.isPartner.isMember) {
      return html`
        <bs-card>
            <bs-card-body>
                <bs-card-title slot="card-title">
                    <h5>Partner ID list</h5>
                </bs-card-title>
                <bs-card-text slot="card-text">
                    <p>From the submitted structures, those list file that have accession number and mapped partner id.</p>
                </bs-card-text>
                <bs-button primary href="#" @click=${this.handleDownload}>Download</bs-button>
            </bs-card-body>
        </bs-card>
      `;
    }
  }
  connectedCallback() {
    super.connectedCallback();
    this.host =  getdomain(location.href);
    const url = 'https://'+ this.host +'/sparqlist/api/gtc_check_partner_member_by_hash_list?user_hash=' + this.userHash;
    this.checkPartner(url);
  }
  checkPartner(url) {
    console.log(url);
    var urls = [];
    urls.push(url);
    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("user-hash");
      console.log(JSON.stringify(myJson));
      return myJson;
    }));
    Promise.all(promises).then(results => {
      this.isPartner = results.pop();
      console.log("this.isPartner.isPartner > " + this.isPartner.isPartner);
    });
  }
  handleDownload() {
    let host =  getdomain(location.href);
    fetch('https://'+ host +'/sparqlist/api/gtc_submissions_partnerid_list_by_user_hash?user_hash=' + this.userHash, {
        method: "GET",
    }).then(response => response.json())
    .then(json => {
      if( !this.isEmpty(json) ) {
        // JSON to CSV with json2csv
        const json2csvParser = new Parser();
        let csv = json2csvParser.parse(json);
        // add GTC version and download date
        const today = new Date();
        let dl_date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
        let header = 'glytoucan.org v' + this.gtcVersion + ' - download date ' + dl_date + '\n';
        csv = header + csv;
        console.log(csv);
        // Create CSV file with Blob
        let blob = new Blob(
          [csv],
          { "type": "text/csv" })
        let link = document.createElement('a')
        link.href = window.URL.createObjectURL(blob)
        link.download = 'partnerid_list.csv';
        // link.download = 'partnerid_list_' + dl_date + '.csv';
        link.click();
      } else {
        console.log('Empty');
        alert('File is Empty. Does not exist accession numbers and partner id in your submission.');
      }
    });
  }
  isEmpty(array){
    return !array.length;
  }
}

customElements.define('gly-partnerid-download', GlyPartneridDownload);
